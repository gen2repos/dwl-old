#include <X11/XF86keysym.h>
/* appearance */
static const int sloppyfocus        = 1;  /* focus follows mouse */
static const unsigned int borderpx  = 2;  /* border pixel of windows */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const float rootcolor[]      = {0.0, 0.0, 0.0, 1.0};
static const float bordercolor[]    = {.42, 0.44, 0.54, 1.0};
static const float focuscolor[]     = {0.52, 0.63, 0.78, 1.0};
static const char cursortheme[]     = "Vimix-White"; /* theme from /usr/share/cursors/xorg-
   x11 */
static const unsigned int cursorsize = 24;

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* app_id     title       tags mask     isfloating   monitor */
	/* examples:
	{ "Gimp",     NULL,       0,            1,           -1 },
	{ "firefox",  NULL,       1 << 8,       0,           -1 },
	*/
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* monitors
 * The order in which monitors are defined determines their position.
 * Non-configured monitors are always added to the left. */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect x y resx resy rate adaptive*/
	/* example of a HiDPI laptop monitor at 120Hz:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 0, 0, 120.000, 1},
	*/
	/* defaults */
	{ NULL,       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL, 0, 0, 0, 0, 144.000, 0},
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	/* example:
	.options = "ctrl:nocaps",
	*/
	.layout = "dvorak",
	.options = "caps:swapescape",
};

static const int repeat_rate = 25;
static const int repeat_delay = 600;

static const char *kblayouts[] = {"us(dvorak)", "fr(bepo_afnor)"};

/* Trackpad */
static const int tap_to_click = 1;
static const int natural_scrolling = 0;

#define MODKEY WLR_MODIFIER_LOGO
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_SHIFT, KEY,            tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,KEY,toggletag,  {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *upvol[] = { "vol", "up", NULL };
static const char *downvol[] = { "vol", "down", NULL };
static const char *mute[] = { "vol", "toggle", NULL };
static const char *forwardmusic[] = { "music", "forward", NULL };
static const char *backwardmusic[] = { "music", "backward", NULL };
static const char *skipmusic[] = { "music", "skip", NULL };
static const char *prevmusic[] = { "music", "previous", NULL };
static const char *termcmd[] = { "kitty", NULL };
static const char *menucmd[] = { "bemenu-run", NULL };

static const Key keys[] = {
	/* modifier                  key          function          argument */
	{ MODKEY,                    36,          spawn,            {.v = termcmd} },
	{ MODKEY,                    40,          spawn,            {.v = menucmd} },
	{ MODKEY,                    33,          spawn,            SHCMD("pass-bemenu") },
	{ MODKEY|WLR_MODIFIER_SHIFT, 33,          spawn,            SHCMD("pass-bemenu -u") },
	{ MODKEY|WLR_MODIFIER_CTRL,  33,          spawn,            SHCMD("pass-bemenu -o") },
	{ MODKEY,                    44,          focusstack,       {.i = +1} },
	{ MODKEY,                    45,          focusstack,       {.i = -1} },
	{ MODKEY,                    34,          incnmaster,       {.i = +1} },
	{ MODKEY,                    35,          incnmaster,       {.i = -1} },
	{ MODKEY,                    43,          setmfact,         {.f = -0.05} },
	{ MODKEY,                    46,          setmfact,         {.f = +0.05} },
	{ MODKEY,                    52,          zoom,             {0} },
	{ MODKEY,                    23,          view,             {0} },
	{ MODKEY,                    53,          killclient,       {0} },
	{ MODKEY,                    59,          setlayout,        {.v = &layouts[0]} },
	{ MODKEY,                    60,          setlayout,        {.v = &layouts[1]} },
	{ MODKEY,                    61,          setlayout,        {.v = &layouts[2]} },
	{ MODKEY|WLR_MODIFIER_SHIFT, 65,          togglefloating,   {0} },
	{ MODKEY,                    41,          togglefullscreen, {0} },
	{ MODKEY,                    25,          focusmon,         {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY,                    26,          focusmon,         {.i = WLR_DIRECTION_RIGHT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, 25,          tagmon,           {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, 26,          tagmon,           {.i = WLR_DIRECTION_RIGHT} },
	{ MODKEY,                    65,          togglekblayout,   {0} },
	{ MODKEY,                    39,          spawn,            SHCMD("scrot") },
	{ 0,                         123,         spawn,            {.v = upvol } },
	{ 0,                         122,         spawn,            {.v = downvol } },
	{ 0,                         121,         spawn,            {.v = mute } },
	{ 0,                         171,         spawn,            {.v = forwardmusic } },
	{ 0,                         172,         spawn,            {.v = skipmusic } },
	{ 0,                         174,         spawn,            {.v = prevmusic } },
	{ 0,                         173,         spawn,            {.v = backwardmusic } },
	TAGKEYS(                     10,                            0),
	TAGKEYS(                     11,                            1),
	TAGKEYS(                     12,                            2),
	TAGKEYS(                     13,                            3),
	TAGKEYS(                     14,                            4),
	TAGKEYS(                     15,                            5),
	TAGKEYS(                     16,                            6),
	TAGKEYS(                     17,                            7),
	TAGKEYS(                     18,                            8),
	{ MODKEY|WLR_MODIFIER_SHIFT, 53,          quit,             {0} },

	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT, 22, quit,             {0} },
#define CHVT(KEY,n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT, KEY, chvt, {.ui = (n)} }
	CHVT(67, 1), CHVT(68, 2), CHVT(69, 3), CHVT(70, 4),  CHVT(71, 5),  CHVT(72, 6),
	CHVT(73, 7), CHVT(74, 8), CHVT(75, 9), CHVT(76, 10), CHVT(95, 11), CHVT(96, 12),
};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
